import 'dart:async';

import 'package:rxdart/rxdart.dart';

class FormBloc {
  final _fullName = BehaviorSubject<String>();
  final _icNumber = BehaviorSubject<String>();
  final _phoneNumber = BehaviorSubject<String>();

  //Get
  Stream<String> get fullName => _fullName.stream.transform(validateFullName);
  Stream<String> get icNumber => _icNumber.stream.transform(validateICNumber);
  Stream<String> get phoneNumber => _phoneNumber.stream.transform(validatePhoneNumber);

  Stream<bool> get formValid => Rx.combineLatest2(_fullName, _icNumber, (_fullName, _icNumber) => true);

  //Set
  Function(String) get changeFullName => _fullName.sink.add;
  Function(String) get changeICNumber => _icNumber.sink.add;
  Function(String) get changePhoneNumber => _phoneNumber.sink.add;

  disppose() {
    _fullName.close();
    _icNumber.close();
     _phoneNumber.close();
  }

  static final icNumberRegex = RegExp(r'^\d{6}-\d{2}-\d{4}$');
  static final phoneNumberRegex = RegExp(r'^(01)[0-46-9]*[0-9]{7,8}$');

  // VALIDATE FULL NAME
  final validateFullName = StreamTransformer<String, String>.fromHandlers(
      handleData: (fullName, sink) {
    if (fullName.length > 2) {
      sink.add(fullName);
    } else {
      sink.addError("Full name must be more than 2 characters");
    }
  });

  // VALIDATE IC NUMBER
  final validateICNumber = StreamTransformer<String, String>.fromHandlers(
      handleData: (icNumber, sink) {
    try {
      if (icNumberRegex.hasMatch(icNumber)) {
        sink.add(icNumber);
      } else {
        sink.addError('Not match');
      }
    } catch (error) {
      sink.addError('Please enter a valid id number');
    }
  });

  // VALIDATE PHONE NUMBER
  final validatePhoneNumber = StreamTransformer<String, String>.fromHandlers(
      handleData: (phoneNumber, sink) {
    try {
      if (phoneNumberRegex.hasMatch(phoneNumber) && phoneNumber.length < 12) {
        print(phoneNumber);
        sink.add(phoneNumber);
      } else {
        sink.addError('Not match');
      }
    } catch (error) {
      sink.addError('Please enter a valid phone number');
    }
  });



  submitProduct() {
    print('User data $fullName & $icNumber');
  }
}
