import 'package:ez_form/form/from_bloc.dart';
import 'package:flutter/material.dart';
import 'package:flutter_masked_text/flutter_masked_text.dart';
import 'package:provider/provider.dart';

void main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return Provider(
      create: (context) => FormBloc(),
      child: MaterialApp(
        title: 'Flutter Demo',
        theme: ThemeData(
          primarySwatch: Colors.blue,
        ),
        home: Home(),
      ),
    );
  }
}

class Home extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    final bloc = Provider.of<FormBloc>(context);

    return Scaffold(
      appBar: AppBar(
        title: Text("FORM"),
      ),
      body: Padding(
        padding: const EdgeInsets.symmetric(horizontal: 20.0),
        child: Column(
          children: <Widget>[
            fullName(bloc),
            icNumber(bloc),
            phoneNumber(bloc),
            saveButton()
          ],
        ),
      ),
    );
  }

  Widget fullName(FormBloc bloc) {
    return StreamBuilder<String>(
        stream: bloc.fullName,
        builder: (context, snapshot) {
          return TextField(
            decoration: InputDecoration(
                labelText: "Full Name", errorText: snapshot.error),
            onChanged: bloc.changeFullName,
          );
        });
  }

  Widget icNumber(FormBloc bloc) {
  var icFormatter = MaskedTextController(mask: '000000-00-0000');
    return StreamBuilder<String>(
        stream: bloc.icNumber,
        builder: (context, snapshot) {
          return TextField(
              controller: icFormatter,
              decoration: InputDecoration(
                  labelText: "IC Number", errorText: snapshot.error),
              onChanged: (data) {
                bloc.changeICNumber;
              });
        });
  }

  Widget phoneNumber(FormBloc bloc) {
    return StreamBuilder<String>(
        stream: bloc.phoneNumber,
        builder: (context, snapshot) {
          return TextField(
            decoration: InputDecoration(
                labelText: "Phone Number", errorText: snapshot.error),
            onChanged: bloc.changePhoneNumber,
          );
        });
  }

  Widget saveButton() {
    return FlatButton(
      color: Colors.blue,
      onPressed: () {},
      child: Text("Save"),
    );
  }
}
